/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MiPaquete;

/**
 *
 * @author Victor
 */
public class User
{

    private String name;
    private String apellido;
    private String nameLogin;
    private String edad;
    private String topicos;

    public String getName()
    {
        return name;
    }

    public String getApellido()
    {
        return apellido;
    }

    public String getNameLogin()
    {
        return nameLogin;
    }

    public String getEdad()
    {
        return edad;
    }

    public String getTopicos()
    {
        return topicos;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setApellido(String apellido)
    {
        this.apellido = apellido;
    }

    public void setNameLogin(String nameLogin)
    {
        this.nameLogin = nameLogin;
    }

    public void setEdad(String edad)
    {
        this.edad = edad;
    }

    public void setTopicos(String topicos)
    {
        this.topicos = topicos;
    }

}
