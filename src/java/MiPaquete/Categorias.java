package MiPaquete;

/**
 *
 * @author Victor
 */
import java.util.Iterator;
import java.util.Vector;

public class Categorias
{

    private Vector categorias = new Vector();

    public Categorias()
    {
        categorias.add("Fechas Históricas");
        categorias.add("Biografías");
        categorias.add("Paisajes");
    }

    public Iterator getAllCategorias()
    {
        return categorias.iterator();
    }
}
