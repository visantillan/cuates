
<%-- 
    Document   : registro
    Created on : 9/10/2014, 07:46:55 AM
    Author     : Victor
--%>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>

    <body>
        <h1>Registro de Usuario</h1>
        <sql:setDataSource var="db" driver="com.mysql.jdbc.Driver"
                           url="jdbc:mysql://localhost/servlet"
                           user="victor"  password="victor2014"/>
        <jsp:useBean id="user" scope="session" class="MiPaquete.User">
            <jsp:setProperty name="user" property="*" />
        </jsp:useBean>
        Los datos proporcionados para hacer su Registro fueron:
        <p>Nombre:  <%= user.getName()%>.</p>
        <p>Apellido:
            <jsp:getProperty name="user" property="apellido" />.</p>
        <p>Nombre de usuario:
            <jsp:getProperty name="user" property="nameLogin" />.</p>
        <p>Edad:
            <jsp:getProperty name="user" property="edad" />.</p>
        Y los topicos seleccionados fueron::
        <%
            String[] topicos = request.getParameterValues("topicos");
            if (topicos == null)
            {
                topicos = new String[]
                {
                    "No ha seleccionado ningun topico"
                };
            }
            for (int i = 0; i < topicos.length; i++)
            {
        %>
        <br><%= topicos[i]%>
        <%
            }
        %>
        <p>Ir a la <a href="index.jsp">Pagina de lista de Topicos</a></p>
        <%@ include file="/WEB-INF/pie.jspf" %>
    </body>


</html>
