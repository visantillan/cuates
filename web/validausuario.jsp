<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : validausuario
    Created on : 13/10/2014, 03:39:19 PM
    Author     : Víctor <viktor.santillan@gmail.com>
--%>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!--Con esta etiqueta hace la conexión a la base de datos dentro de un JSP-->
        <sql:setDataSource var="db" driver="com.mysql.jdbc.Driver"
                           url="jdbc:mysql://localhost/servlet"
                           user="victor"  password="victor2014"/>
        <%
            String usuario = request.getParameter("usuario");
            String password = request.getParameter("password");
        %>

        <!--Con esta etiqueta hace la consulta a la base de datos dentro de un JSP-->
        <sql:query dataSource="${db}" var="resultado">
            SELECT * from cuates where nombre = "<%= usuario%>" and password = "<%= password%>";
        </sql:query>
        <!--Con esta etiqueta valida si existe o no un usuario registrado-->
        <c:choose>
            <c:when test="${resultado.rowCount == 0}">
                <p>No existes</p>
                <p>Por favor registrese en la siguiente página:</p>
                <a href="registro.jsp">Registro</a>
            </c:when>
            <c:otherwise>
                <p>Bienvenido a la cueva de los cuates</p>
            </c:otherwise>
        </c:choose>

    </body>
</html>
