
<%-- 
    Document   : index
    Created on : 9/10/2014, 07:16:57 AM
    Author     : Victor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="/WEB-INF errorPage.jsp" import="java.util.Iterator, MiPaquete.*"  %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Pagina de Bienvenida</h1>
        <%
            User user = (User) session.getAttribute("user");
            if (user == null)
            {
        %>
        Si Usted no ha sido registrado por favor haga click 
        <a href="formaRegistro.html">aqui­ </a>.
        <%
        } else
        {
        %>
        Lista de Categorias
        <%! Categorias cates = new Categorias(); %>
        Haga Click sobre una liga de las mostradas abajo.
        <%
            Iterator categorias = cates.getAllCategorias();
            while (categorias.hasNext())
            {
                String categoria = (String) categorias.next();
        %>
        <p>
            <a href="<%= replaceUnderscore(categoria)%>.jsp"><%= categoria%>
            </a></p>
            <%
                }
            %>
            <%@ include file="/WEB-INF/pie.jspf" %>
            <%
                }
            %>

    </body>
</html>
<%!

    public String replaceUnderscore(String s)
    {
        return s.replace(' ', '_');
    }
%>

